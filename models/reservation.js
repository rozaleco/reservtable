"use strict";

const bcrypt = require('bcryptjs');

module.exports = function(sequelize, DataTypes) {
  return sequelize.define("reservations", {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    restaurantID: DataTypes.INTEGER,
    reservationDate: DataTypes.DATE,
    attendCount: DataTypes.INTEGER,
    phoneNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    status: DataTypes.STRING
  });
};
