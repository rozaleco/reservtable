"use strict";

const bcrypt = require('bcryptjs');

module.exports = function (sequelize, DataTypes) {
    return sequelize.define("restaurant", {
        restaurantName: DataTypes.STRING,
        restaurantAddress: DataTypes.STRING,
        restaurantCity: DataTypes.STRING,
        restaurantType: DataTypes.STRING,
        restaurantDescription: DataTypes.STRING,
        twoSeatTables: DataTypes.INTEGER,
        fourSeatTables: DataTypes.INTEGER,
        sixSeatTables: DataTypes.INTEGER,
        openingHrs: DataTypes.STRING,
        restaurantImage: DataTypes.STRING,
        email: {type: DataTypes.STRING, unique: true},
        phoneNumber: DataTypes.STRING,
    });
};
