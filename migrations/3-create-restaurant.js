"use strict";

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface
            .createTable('restaurant', {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true,
                    allowNull: false
                },
                restaurantName: Sequelize.STRING,
                restaurantAddress: Sequelize.STRING,
                restaurantCity: Sequelize.STRING,
                restaurantType: Sequelize.STRING,
                restaurantDescription: Sequelize.STRING,
                openingHrs: Sequelize.STRING,
                twoSeatTables: Sequelize.INTEGER,
                fourSeatTables: Sequelize.INTEGER,
                sixSeatTables: Sequelize.INTEGER,
                restaurantImage: Sequelize.STRING,
                email: {type: Sequelize.STRING, unique: true},
                phoneNumber: Sequelize.STRING,
                createdAt: {
                    type: Sequelize.DATE,
                    allowNull: false
                },
                updatedAt: Sequelize.DATE
            });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface
            .dropTable('restaurant');
    }
};
