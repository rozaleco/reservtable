"use strict";

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface
      .createTable('reservations', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false
        },
        firstName: Sequelize.STRING,
        lastName: Sequelize.STRING,
        restaurantID: Sequelize.INTEGER,
        reservationDate: Sequelize.DATE,
        attendCount: Sequelize.INTEGER,
        phoneNumber: Sequelize.STRING,
        email: Sequelize.STRING,
        status: Sequelize.STRING,
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: Sequelize.DATE
      });
  },

  down: function(queryInterface, Sequelize) {
    return queryInterface
      .dropTable('reservations');
  }
};
